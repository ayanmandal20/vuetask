import task1 from './components/task1.vue'
import task2 from './components/task2.vue'
import task3 from './components/task3.vue'
import task4 from './components/task4.vue'
import parent from './components/task5/parent.vue'
import parent1 from './components/task6/parent1.vue'
import task7 from './components/task7.vue'

export default [
    {path:'/', component: task1},
    {path:'/task2', component: task2},
    {path:'/task3', component: task3},
    {path:'/task4', component: task4},
    {path:'/task5', component: parent},
    {path:'/task6', component: parent1},
    {path:'/task7', component: task7}
]